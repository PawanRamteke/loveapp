//
//  General.h
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#ifndef General_h
#define General_h

#define FONT_ROBOT_BOLD(size1)               [UIFont fontWithName:@"Dubai-Bold" size:size1]

#define FONT_ROBOT_REGULAR(size1)     [UIFont fontWithName:@"Dubai-Regular" size:size1]

#define FONT_ROBOT_MEDIUM(size1)     [UIFont fontWithName:@"Dubai-Medium" size:size1]

#define CONST_PAGE_WIDTH    (IS_IPHONE ? 375 : 768)

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define VIEWMANAGER                [ViewManager getManager]
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

#define    CATEGORY_WALLPAPER  0
#define    CATEGORY_QUOTES 1

#define ADMOB_ID            @"ca-app-pub-9799504098762666~4034876722"
#define ADMOB_BANNER_ID     @"ca-app-pub-9799504098762666/8990454466"
#define ADMOB_INTERSTITIAL_AD_ID @"ca-app-pub-9799504098762666/2195181440"


#define ENGLISH     @"ENG"
#define HINDI       @"HINDI"
#define SPANISH     @"SPAIN"

#define REMOTE_API      [[RemoteAPI alloc]init]
#define API_TOKEN       @"we!nSp!re@786"
#define APP_NAME        @"LOVE"

#define BASE_URL                @"http://www.weinspire.in/love_app/public/api/v1/"
#define WALLPAPER_CATEGORY_URL  @"wallpaper_category"
#define QUOTES_CATEGORY_URL     @"quotes_category"
#define QUOTES_LIST_URL         @"quotes_by_category"
#define WALLPAPER_LIST_URL      @"wallpaper_by_category"
#define FAVOURITE_URL           @"favorites"











#endif /* General_h */
