//
//  CoreDataHandler.m
//  Love App
//
//  Created by Pawan Ramteke on 17/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "CoreDataHandler.h"
#import "AppDelegate.h"

@implementation CoreDataHandler
+(void)addWallpaperToFavourite:(double)category_id
{
    FavoriteWallpaper *favWallpaper = nil;
    favWallpaper = [self getFavWallpaperByCategoryId:category_id];
    
    if (favWallpaper == nil)
    {
        NSEntityDescription* entityDescription = [NSEntityDescription entityForName:NSStringFromClass([FavoriteWallpaper class]) inManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
        favWallpaper = [[FavoriteWallpaper alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
    }
    favWallpaper.wallpaperId = category_id;
    [self saveAndUpdate];
    
}
+(void)removeWallpaperFromFavourite:(double)category_id
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([FavoriteWallpaper class]) inManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"wallpaperId == %lf",category_id];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *items = [VIEWMANAGER.appManagedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject *managedObject in items)
    {
        [VIEWMANAGER.appManagedObjectContext deleteObject:managedObject];
    }
}

+(NSArray *)getAllFavouriteWallpapers
{
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:NSStringFromClass([FavoriteWallpaper class])];
    
    NSError *error = nil;
    
    NSArray *results = [VIEWMANAGER.appManagedObjectContext executeFetchRequest:request error:&error];
    NSMutableArray *dataToSend = [NSMutableArray new];
    
    for (FavoriteWallpaper *model in results) {
        [dataToSend addObject:@(model.wallpaperId).stringValue];
    }
    return dataToSend.copy;
}

+(FavoriteWallpaper *)getFavWallpaperByCategoryId:(double)cat_id
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"wallpaperId == %lf",cat_id];
    
    NSArray *result = [self findDataByPredicate:predicate withClass:[FavoriteWallpaper class]];
    FavoriteWallpaper *model = nil;
    if (result.count) {
        model = [result objectAtIndex:0];
    }
    return model;
}

+(void)addQuoteToFavourite:(double)category_id
{
    FavoriteQuote *favWallpaper = nil;
    favWallpaper = [self getFavQuoteByCategoryId:category_id];
    
    if (favWallpaper == nil)
    {
        NSEntityDescription* entityDescription = [NSEntityDescription entityForName:NSStringFromClass([FavoriteQuote class]) inManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
        favWallpaper = [[FavoriteQuote alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
    }
    favWallpaper.quoteId = category_id;
    [self saveAndUpdate];
}
+(void)removeQuoteFromFavourite:(double)category_id
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([FavoriteQuote class]) inManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"quoteId == %lf",category_id];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *items = [VIEWMANAGER.appManagedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject *managedObject in items)
    {
        [VIEWMANAGER.appManagedObjectContext deleteObject:managedObject];
    }
}

+(NSArray *)getAllFavouriteQuotes
{
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:NSStringFromClass([FavoriteQuote class])];
    
    NSError *error = nil;
    
    NSArray *results = [VIEWMANAGER.appManagedObjectContext executeFetchRequest:request error:&error];
    NSMutableArray *dataToSend = [NSMutableArray new];
    
    for (FavoriteQuote *model in results) {
        [dataToSend addObject:@(model.quoteId).stringValue];
    }
    return dataToSend.copy;
}


+(NSArray *)findDataByPredicate:(NSPredicate *)predicate withClass:(Class)class
{
    NSFetchRequest *req = [[NSFetchRequest alloc] init];
    req.predicate = predicate;
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass(class) inManagedObjectContext:VIEWMANAGER.appManagedObjectContext];
    req.entity = entityDescription;
    NSArray *result = [VIEWMANAGER.appManagedObjectContext executeFetchRequest:req error:nil];
    return result;
}

+(FavoriteQuote *)getFavQuoteByCategoryId:(double)cat_id
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"quoteId == %lf",cat_id];
    
    NSArray *result = [self findDataByPredicate:predicate withClass:[FavoriteWallpaper class]];
    FavoriteQuote *model = nil;
    if (result.count) {
        model = [result objectAtIndex:0];
    }
    return model;
}

+(void)saveAndUpdate
{
    NSError *error = nil;
    [VIEWMANAGER.appManagedObjectContext save:&error];
}

@end
