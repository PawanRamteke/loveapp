//
//  ViewManager.m
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "ViewManager.h"
#import "SVProgressHUD.h"

@implementation ViewManager
+ (ViewManager *)getManager
{
    static ViewManager *viewManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        viewManager = [[self alloc] init];
    });
    return viewManager;
}

- (CGFloat)getFlexibleHeight:(CGFloat)height
{
    CGFloat flexibleheight;
    flexibleheight = height * (SCREEN_WIDTH/CONST_PAGE_WIDTH);
    return flexibleheight;
}

-(void)showAcytivityIndicator
{
    NSLog(@"Show======>");
    
    // [SVProgressHUD sharedView].strokeColorForApp = [UIColor whiteColor];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD setRingThickness:2];
  //  [SVProgressHUD setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
    [SVProgressHUD showWithStatus:@"Please Wait.."];
    
    
    //self.indicatorCount++;
    
}
-(void)hideAcytivityIndicator
{
    NSLog(@"Hide======>");
    
    [SVProgressHUD dismiss];
}

- (UIViewController *) topMostController
{
    UIViewController *topViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (true)
    {
        if (topViewController.presentedViewController) {
            topViewController = topViewController.presentedViewController;
        } else if ([topViewController isKindOfClass:[UINavigationController class]]) {
            UINavigationController *nav = (UINavigationController *)topViewController;
            topViewController = nav.topViewController;
        } else if ([topViewController isKindOfClass:[UITabBarController class]]) {
            UITabBarController *tab = (UITabBarController *)topViewController;
            topViewController = tab.selectedViewController;
        } else {
            break;
        }
    }
    
    return topViewController;
}


-(void)presentActivityControllerWithItem:(NSArray *)items
{
    UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    [self presentActivityController:controller];
}

- (void)presentActivityController:(UIActivityViewController *)controller {
    
    controller.modalPresentationStyle = UIModalPresentationPopover;
    [[self topMostController] presentViewController:controller animated:YES completion:nil];
    
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    //popController.barButtonItem = self.navigationItem.leftBarButtonItem;
    
    controller.completionWithItemsHandler = ^(NSString *activityType,BOOL completed,NSArray *returnedItems,NSError *error){
        if (completed) {
            NSLog(@"We used activity type%@", activityType);
        } else {
            NSLog(@"We didn't want to share anything after all.");
        }
        
        if (error) {
            NSLog(@"An Error occured: %@, %@", error.localizedDescription, error.localizedFailureReason);
        }
    };
}

-(void)playMusic
{
    if (!self.audioPlayer) {
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"love_music" ofType:@"mp3"]];
        self.audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:url error:nil];
        self.audioPlayer.numberOfLoops = -1;
    }
    [self.audioPlayer play];
}
-(void)pauseMusic
{
    [self.audioPlayer pause];
}

@end
