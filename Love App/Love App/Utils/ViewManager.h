//
//  ViewManager.h
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface ViewManager : NSObject

@property (nonatomic,assign)BOOL isPlayMusic;
@property (nonatomic,strong)AVAudioPlayer *audioPlayer;
@property(nonatomic,strong) NSManagedObjectContext *appManagedObjectContext;

+ (ViewManager *)getManager;
- (CGFloat)getFlexibleHeight:(CGFloat)height;
-(void)showAcytivityIndicator;
-(void)hideAcytivityIndicator;
- (UIViewController *) topMostController;
-(void)presentActivityControllerWithItem:(NSArray *)items;
-(void)playMusic;
-(void)pauseMusic;
@end
