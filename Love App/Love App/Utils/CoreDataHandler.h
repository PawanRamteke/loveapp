//
//  CoreDataHandler.h
//  Love App
//
//  Created by Pawan Ramteke on 17/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "FavoriteWallpaper+CoreDataProperties.h"
#import "FavoriteQuote+CoreDataProperties.h"

@interface CoreDataHandler : NSObject
+(void)addWallpaperToFavourite:(double)category_id;
+(void)removeWallpaperFromFavourite:(double)category_id;
+(FavoriteWallpaper *)getFavWallpaperByCategoryId:(double)cat_id;

+(void)addQuoteToFavourite:(double)category_id;
+(void)removeQuoteFromFavourite:(double)category_id;

+(NSArray *)getAllFavouriteWallpapers;
+(NSArray *)getAllFavouriteQuotes;

@end
