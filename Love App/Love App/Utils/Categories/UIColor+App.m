//
//  UIColor+App.m
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "UIColor+App.h"

@implementation UIColor (App)

+(NSArray *)buttonFavouriteGradientColors
{
    return @[(id)[self colorFromHexString:@"1eb1bb"].CGColor,(id)[self colorFromHexString:@"c5ea60"].CGColor];
}

+(NSArray *)buttonWallpaperGradientColors
{
    return @[(id)[self colorFromHexString:@"eca74a"].CGColor,(id)[self colorFromHexString:@"d5175a"].CGColor];
}

+(NSArray *)buttonQuotesGradientColors
{
    return @[(id)[self colorFromHexString:@"169c41"].CGColor,(id)[self colorFromHexString:@"6bc933"].CGColor];
}

+(NSArray *)shareViewGradientColors
{
    return @[(id)[self colorFromHexString:@"E41D75"].CGColor,(id)[self colorFromHexString:@"6B2A87"].CGColor];
}


+ (UIColor *)colorFromHexString:(NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    
    NSLog(@"colorString :%@",colorString);
    CGFloat alpha, red, blue, green;
    
    // #RGB
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
+(CGFloat)colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

@end
