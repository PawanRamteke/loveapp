//
//  UIColor+App.h
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (App)

+(NSArray *)buttonFavouriteGradientColors;
+(NSArray *)buttonWallpaperGradientColors;
+(NSArray *)buttonQuotesGradientColors;
+(NSArray *)shareViewGradientColors;
@end
