//
//  UIFont+App.m
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "UIFont+App.h"
@implementation UIFont (App)
+ (UIFont *)appFontRegular:(CGFloat)size
{
    return FONT_ROBOT_REGULAR([VIEWMANAGER getFlexibleHeight:size]);
}

+ (UIFont *)appFontBold:(CGFloat)size
{
    return FONT_ROBOT_BOLD([VIEWMANAGER getFlexibleHeight:size]);
}
@end
