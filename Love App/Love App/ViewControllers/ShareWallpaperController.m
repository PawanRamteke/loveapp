//
//  ShareWallpaperController.m
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "ShareWallpaperController.h"
#import "UIImageView+AFNetworking.h"
#import "WallpaperCollectionCell.h"
#import "CoreDataHandler.h"
#import "FavoriteWallpaper+CoreDataProperties.h"

@interface ShareWallpaperController ()<UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate>
{
    __weak IBOutlet UIView *viewTop;
    __weak IBOutlet UIImageView *imgView;
    NSInteger selectedIndex;
    
    UICollectionView *_collectionView;
    __weak IBOutlet UIView *viewCount;
    __weak IBOutlet UILabel *lblCount;
    __weak IBOutlet UIButton *btnFavourite;
    __weak IBOutlet UIButton *btnShare;
    NSTimer *timer;
    BOOL isTap;
}
@end

@implementation ShareWallpaperController

- (void)viewDidLoad {
    [super viewDidLoad];
    isTap = YES;
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    _collectionView = [[UICollectionView alloc]initWithFrame:self.view.frame collectionViewLayout:layout];
    [_collectionView registerClass:[WallpaperCollectionCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView setBackgroundColor:[UIColor clearColor]];
    _collectionView.pagingEnabled = YES;
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    _collectionView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:_collectionView];
    
    [_collectionView enableAutolayout];
    [_collectionView leadingMargin:0];
    [_collectionView trailingMargin:0];
    [_collectionView topMargin:-20];
    [_collectionView bottomMargin:0];
    
    [_collectionView performBatchUpdates:^{
        
    } completion:^(BOOL finished) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:self.selIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
        });
    }];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];

    tapGesture.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapGesture];
    
    selectedIndex = self.selIndex;
    
    
    
    [btnFavourite setImage:[UIImage imageNamed:@"favourite_icon"] forState:UIControlStateNormal];
    [btnFavourite setImage:[UIImage imageNamed:@"favorite_fill"] forState:UIControlStateSelected];
    
    WallpaperListModel *model = _arrWallpaperList[selectedIndex];
    FavoriteWallpaper *favModel = [CoreDataHandler getFavWallpaperByCategoryId:model.internalBaseClassIdentifier];
    
    btnFavourite.selected = favModel == nil ? NO : YES;
    
  //  lblCount.text = [NSString stringWithFormat:@"%lu/%ld",selectedIndex+1,(unsigned long)_arrWallpaperList.count];
    
    [self setImageWithIndex:selectedIndex];
    
    [self.view bringSubviewToFront:viewTop];
 //   [self.view bringSubviewToFront:viewCount];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _arrWallpaperList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    WallpaperCollectionCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    WallpaperListModel *model = _arrWallpaperList[indexPath.row];
    
    NSURL *url = [NSURL URLWithString:model.url];
    NSString *last = [NSString stringWithFormat:@"thumb/%@",[url lastPathComponent]];
    url =  [url URLByDeletingLastPathComponent];
    url = [url URLByAppendingPathComponent:last];
    
    // lblCount.text = [NSString stringWithFormat:@"%lu/%ld",indexPath.row,(unsigned long)_arrWallpaperList.count];
    
    [cell.imgView setImageWithURL:[NSURL URLWithString:model.url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ShareWallpaperController *shareWallpaperVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareWallpaperController"];
    shareWallpaperVC.arrWallpaperList = _arrWallpaperList;
    shareWallpaperVC.selIndex = indexPath.row;
    [self.navigationController pushViewController:shareWallpaperVC animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(SCREEN_WIDTH , SCREEN_HEIGHT);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int page = scrollView.contentOffset.x / scrollView.frame.size.width;
    self.selIndex = page;
    lblCount.text = [NSString stringWithFormat:@"%lu/%ld",self.selIndex+1,(unsigned long)_arrWallpaperList.count];
    
    WallpaperListModel *model = _arrWallpaperList[self.selIndex];
    FavoriteWallpaper *favModel = [CoreDataHandler getFavWallpaperByCategoryId:model.internalBaseClassIdentifier];
    
    btnFavourite.selected = favModel == nil ? NO : YES;
}


- (IBAction)btnShareClicked:(id)sender {
    
    WallpaperCollectionCell *cell =(WallpaperCollectionCell *)[_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:self.selIndex inSection:0]];
    NSArray *dataToShare =@[cell.imgView.image];
    [VIEWMANAGER presentActivityControllerWithItem:dataToShare];
}

- (IBAction)btnFavouriteClicked:(UIButton *)sender {
    sender.selected = !sender.selected;
    
    WallpaperListModel *model = _arrWallpaperList[self.selIndex];

    if (sender.selected) {
        [CoreDataHandler addWallpaperToFavourite:model.internalBaseClassIdentifier];
    }
    else{
        [CoreDataHandler removeWallpaperFromFavourite:model.internalBaseClassIdentifier];
    }
}


- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
        NSLog(@"Left Swipe");
        
        if (selectedIndex == self.arrWallpaperList.count-1) {
            return;
        }
        
        selectedIndex = selectedIndex+1;
        [self setImageWithIndex:selectedIndex];
    }
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
        
        if (selectedIndex == 0) {
            return;
        }
        selectedIndex = selectedIndex-1;
        [self setImageWithIndex:selectedIndex];
    }
    
    isTap = YES;
    viewTop.alpha = 1;
    viewCount.alpha = 1;
    lblCount.text = [NSString stringWithFormat:@"%lu/%ld",selectedIndex+1,(unsigned long)_arrWallpaperList.count];
    
}

-(void)handleTap:(UITapGestureRecognizer *)tap
{
    isTap = !isTap;
    [UIView animateWithDuration:0.5 animations:^{
        viewTop.alpha = isTap ? 1 : 0;
        viewCount.alpha = viewTop.alpha;
    }];
}

-(void)setImageWithIndex:(NSInteger)idx
{
    WallpaperListModel *model = self.arrWallpaperList[idx];
    [imgView setImageWithURL:[NSURL URLWithString:model.url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
}


@end
