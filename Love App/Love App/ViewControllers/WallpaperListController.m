//
//  WallpaperListController.m
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "WallpaperListController.h"
#import "WallpaperListView.h"

@interface WallpaperListController ()
{
    __weak IBOutlet UIView *viewTop;
    __weak IBOutlet UIButton *btnMusic;
}
@end

@implementation WallpaperListController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    btnMusic.selected = VIEWMANAGER.isPlayMusic;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setTopViewColor];
    
    [btnMusic setImage:[[UIImage imageNamed:@"music_on"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [btnMusic setImage:[[UIImage imageNamed:@"music_off"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateSelected];
    btnMusic.tintColor = [UIColor whiteColor];
    
    WallpaperListView *wallpaperList = [[WallpaperListView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(viewTop.frame), SCREEN_WIDTH, SCREEN_HEIGHT - CGRectGetMaxY(viewTop.frame) - 60 ) categoryId:self.category_id];
    [self.view addSubview:wallpaperList];
    
}

- (IBAction)btnMusicClicked:(UIButton *)sender {
    sender.selected = !sender.selected;
    VIEWMANAGER.isPlayMusic = sender.selected;
    sender.selected ? [VIEWMANAGER pauseMusic] : [VIEWMANAGER playMusic];
}


-(void)setTopViewColor
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = viewTop.bounds;
    gradient.colors = [UIColor buttonWallpaperGradientColors];
    [viewTop.layer insertSublayer:gradient atIndex:0];
}

@end
