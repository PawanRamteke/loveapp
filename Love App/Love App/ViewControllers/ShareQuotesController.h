//
//  ShareQuotesController.h
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "BaseViewController.h"

@interface ShareQuotesController : BaseViewController
@property (nonatomic, assign)NSInteger selectedIdx;
@property (nonatomic,strong)NSArray *arrQuotes;
@property (weak, nonatomic) IBOutlet UILabel *lblQuote;
@end
