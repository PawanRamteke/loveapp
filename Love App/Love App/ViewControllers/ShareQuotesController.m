//
//  ShareQuotesController.m
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "ShareQuotesController.h"

@interface ShareQuotesController ()
{
    __weak IBOutlet UILabel *lblCount;
    __weak IBOutlet UIView *viewTop;
    __weak IBOutlet UIButton *btnLeft;
    __weak IBOutlet UIButton *btnRight;
    NSInteger selIndex;
}
@end

@implementation ShareQuotesController

- (void)viewDidLoad {
    [super viewDidLoad];
    viewTop.backgroundColor = [UIColor clearColor];
    [self setViewColor];
    QuotesListModel *model = self.arrQuotes[self.selectedIdx];
    _lblQuote.text = model.quote;
    lblCount.text = [NSString stringWithFormat:@"%ld/%ld",_selectedIdx+1,(unsigned long)_arrQuotes.count];
    
    selIndex = _selectedIdx;
    if (selIndex == 0) {
        btnLeft.hidden = YES;
    }
    if (selIndex == _arrQuotes.count) {
        btnRight.hidden = YES;
    }
}


- (IBAction)btnWhatsAppClicked:(id)sender {
    QuotesListModel *model = _arrQuotes[selIndex];
    NSString * msg = model.quote;
    NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?text=%@",msg];
    NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    } else {
        // Cannot open whatsapp
    }
}

- (IBAction)btnFavoriteClicked:(id)sender {
}

- (IBAction)btnCopyClicked:(id)sender {
}

- (IBAction)btnShareClicked:(id)sender {
    QuotesListModel *model = _arrQuotes[selIndex];
    NSArray *items = @[model.quote];
    
    [VIEWMANAGER presentActivityControllerWithItem:items];
}


- (IBAction)btnRightClicked:(id)sender {
    
    selIndex = selIndex+1;
    btnLeft.hidden = NO;
    if (selIndex == _arrQuotes.count-1) {
        btnRight.hidden = YES;
        selIndex = _arrQuotes.count - 1;
    }
    
    QuotesListModel *model = _arrQuotes[selIndex];
    _lblQuote.text = model.quote;
    lblCount.text = [NSString stringWithFormat:@"%lu/%ld",selIndex+1,(unsigned long)_arrQuotes.count];

}
- (IBAction)btnLeftClicked:(id)sender {
    
    if (selIndex < _arrQuotes.count) {
        btnRight.hidden = NO;
    }
    
    if (selIndex == 1) {
        btnLeft.hidden = YES;
        selIndex = 0;
    }
    else{
        btnLeft.hidden = NO;
        selIndex = selIndex-1;
    }
    
    QuotesListModel *model = _arrQuotes[selIndex];
    _lblQuote.text = model.quote;
    lblCount.text = [NSString stringWithFormat:@"%lu/%ld",selIndex+1,(unsigned long)_arrQuotes.count];

}


-(void)setViewColor
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = [UIColor shareViewGradientColors];
    [self.view.layer insertSublayer:gradient atIndex:0];
}



@end
