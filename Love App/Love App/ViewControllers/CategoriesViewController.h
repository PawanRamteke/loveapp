//
//  CategoriesViewController.h
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
@interface CategoriesViewController : BaseViewController
@property (nonatomic,assign)NSInteger categoryType;
@end
