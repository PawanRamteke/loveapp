//
//  CategoriesViewController.m
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "CategoriesViewController.h"
#import "CategoriesTableCell.h"
#import "WallpaperListController.h"
#import "QuotesListController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>

@interface CategoriesViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    __weak IBOutlet UIView *viewTop;
    __weak IBOutlet UITableView *tblViewCategories;
    NSArray *arrCategoriesList;
    __weak IBOutlet UIButton *btnMusic;
    
}
@property(nonatomic, strong) GADInterstitial *interstitial;
@property(nonatomic, strong) GADBannerView *bannerView;

@end

@implementation CategoriesViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    btnMusic.selected = VIEWMANAGER.isPlayMusic;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    if (self.categoryType == CATEGORY_WALLPAPER) {
        self.navigationTitle.text = @"Wallpapers";
        [self setTopViewColor:[UIColor buttonWallpaperGradientColors]];
    }
    else{
        self.navigationTitle.text = @"Quotes";
        [self setTopViewColor:[UIColor buttonQuotesGradientColors]];
    }
    
    tblViewCategories.estimatedRowHeight = 60;
    tblViewCategories.rowHeight = UITableViewAutomaticDimension;
    
    [self getCategoryList:self.categoryType == CATEGORY_WALLPAPER ? WALLPAPER_CATEGORY_URL : QUOTES_CATEGORY_URL];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self InitializeAdWithView];
    });
    
    [btnMusic setImage:[[UIImage imageNamed:@"music_on"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [btnMusic setImage:[[UIImage imageNamed:@"music_off"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateSelected];
    btnMusic.tintColor = [UIColor whiteColor];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrCategoriesList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CategoriesTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellId"];
    if (cell == nil) {
        cell = [[CategoriesTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellId"];
    }
    
    CategoryModel *model = arrCategoriesList[indexPath.row];
    cell.lblTitle.text = model.name;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryModel *model = arrCategoriesList[indexPath.row];
    
    if (_categoryType == CATEGORY_WALLPAPER) {
        WallpaperListController *wallpaperVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WallpaperListController"];
        wallpaperVC.category_id = model.internalBaseClassIdentifier;
        [self.navigationController pushViewController:wallpaperVC animated:YES];
    }
    else{
        QuotesListController *quotesListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"QuotesListController"];
        quotesListVC.category_id = model.internalBaseClassIdentifier;
        [self.navigationController pushViewController:quotesListVC animated:YES];

    }
}

-(void)getCategoryList:(NSString *)apiName;
{
    NSDictionary *param = @{
        @"api_token" : API_TOKEN,
        @"app_name": APP_NAME,
        @"language": ENGLISH
        };
    
    [VIEWMANAGER showAcytivityIndicator];
    [REMOTE_API CallPOSTWebServiceWithParam:apiName params:param sBlock:^(id responseObject) {
        arrCategoriesList = responseObject;
        [tblViewCategories reloadData];
        [VIEWMANAGER hideAcytivityIndicator];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [VIEWMANAGER hideAcytivityIndicator];
    }];
}

- (IBAction)btnMusicClicked:(UIButton *)sender {
    sender.selected = !sender.selected;
    VIEWMANAGER.isPlayMusic = sender.selected;
    sender.selected ? [VIEWMANAGER pauseMusic] : [VIEWMANAGER playMusic];
}


-(void)setTopViewColor:(NSArray *)gradientColors
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = viewTop.bounds;
    gradient.colors = gradientColors;
    [viewTop.layer insertSublayer:gradient atIndex:0];
}


-(void)InitializeAdWithView
{
    self.interstitial = [[GADInterstitial alloc]
                         initWithAdUnitID:ADMOB_INTERSTITIAL_AD_ID];
    self.interstitial.delegate = self;
    GADRequest *request = [GADRequest request];
    [self.interstitial loadRequest:request];
    
    
    self.bannerView = [[GADBannerView alloc]
                       initWithAdSize:kGADAdSizeBanner];
    self.bannerView.frame = CGRectMake((self.view.frame.size.width - self.bannerView.frame.size.width)/2, self.view.frame.size.height - self.bannerView.frame.size.height, self.bannerView.frame.size.width, self.bannerView.frame.size.height);
    [self.view addSubview:self.bannerView];
    self.bannerView.delegate = self;
    self.bannerView.adUnitID = ADMOB_BANNER_ID;
    self.bannerView.rootViewController = self;
    [self.bannerView loadRequest:[GADRequest request]];
}

#pragma mark - AdMob delegate methods
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    NSLog(@"interstitialDidReceiveAd");
}

- (void)interstitial:(GADInterstitial *)ad
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"interstitial:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}

- (void)interstitialWillPresentScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillPresentScreen");
}

- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillDismissScreen");
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialDidDismissScreen");
}

- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad {
    NSLog(@"interstitialWillLeaveApplication");
}


- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    NSLog(@"adViewDidReceiveAd");
}

- (void)adView:(GADBannerView *)adView
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"adView:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}


- (void)adViewWillPresentScreen:(GADBannerView *)adView {
    NSLog(@"adViewWillPresentScreen");
}

- (void)adViewWillDismissScreen:(GADBannerView *)adView {
    NSLog(@"adViewWillDismissScreen");
}

- (void)adViewDidDismissScreen:(GADBannerView *)adView {
    NSLog(@"adViewDidDismissScreen");
}


- (void)adViewWillLeaveApplication:(GADBannerView *)adView {
    NSLog(@"adViewWillLeaveApplication");
}


@end
