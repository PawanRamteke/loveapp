//
//  ShareWallpaperController.h
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "BaseViewController.h"

@interface ShareWallpaperController : BaseViewController
@property (nonatomic,strong)NSArray *arrWallpaperList;
@property (nonatomic,assign)NSInteger selIndex;
@end
