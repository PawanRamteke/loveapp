//
//  FavouritesViewController.m
//  Love App
//
//  Created by Pawan Ramteke on 10/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "FavouritesViewController.h"
#import "HMSegmentedControl.h"
#import "WallpaperListView.h"
#import "QuotesListView.h"
@interface FavouritesViewController ()<UIScrollViewDelegate>
{
    
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UIView *viewTop;
    HMSegmentedControl *segmentedControl;
    UIScrollView *baseScroll;
}
@end

@implementation FavouritesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    lblTitle.text = @"Favorites";
    self.view.backgroundColor = [UIColor whiteColor];
    [self setTopViewColor:[UIColor buttonWallpaperGradientColors]];
   
    NSArray *items = @[@"Wallpaper",@"Quotes"];
    
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:items];
    segmentedControl.frame = CGRectMake(0, CGRectGetMaxY(viewTop.frame), self.view.frame.size.width, 60);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];
    
    CGFloat yAxis = CGRectGetMaxY(segmentedControl.frame);
    
    baseScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0,yAxis , self.view.frame.size.width, self.view.frame.size.height - yAxis - 60)];
    [self.view addSubview:baseScroll];
    baseScroll.pagingEnabled = YES;
    baseScroll.delegate = self;
    baseScroll.contentSize = CGSizeMake(SCREEN_WIDTH*items.count, 0);
    
    WallpaperListView *wallpaperList = [[WallpaperListView alloc]initWithFrame:CGRectMake(0, 0, baseScroll.frame.size.width, baseScroll.frame.size.height)];
    [baseScroll addSubview:wallpaperList];
    
    QuotesListView *quotesList = [[QuotesListView alloc]initWithFrame:CGRectMake(baseScroll.frame.size.width, 0, baseScroll.frame.size.width,baseScroll.frame.size.height)];
    [baseScroll addSubview:quotesList];
    
}

-(void)segmentedControlChangedValue:(HMSegmentedControl *)segmentControl
{
    [baseScroll setContentOffset:CGPointMake(SCREEN_WIDTH * segmentControl.selectedSegmentIndex, 0) animated:YES];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    int page = scrollView.contentOffset.x / scrollView.frame.size.width;
    [segmentedControl setSelectedSegmentIndex:page animated:YES];
    
}


-(void)setTopViewColor:(NSArray *)gradientColors
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = viewTop.bounds;
    gradient.colors = gradientColors;
    [viewTop.layer insertSublayer:gradient atIndex:0];
}
@end
