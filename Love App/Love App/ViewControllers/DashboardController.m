//
//  DashboardController.m
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "DashboardController.h"
#import "DashboardButton.h"
#import "CategoriesViewController.h"
#import "FavouritesViewController.h"

@interface DashboardController ()
{
    __weak IBOutlet UIButton *btnMusic;
}
@end

@implementation DashboardController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    btnMusic.selected = VIEWMANAGER.isPlayMusic;
}

- (void)viewDidLoad {
    
    [VIEWMANAGER playMusic];
    
    [super viewDidLoad];
    
    [btnMusic setImage:[[UIImage imageNamed:@"music_on"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [btnMusic setImage:[[UIImage imageNamed:@"music_off"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateSelected];
    btnMusic.tintColor = [UIColor blackColor];
}
- (IBAction)btnFavouriteClicked:(DashboardButton *)sender {
    FavouritesViewController *favoritesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FavouritesViewController"];
    [self.navigationController pushViewController:favoritesVC animated:YES];
}

- (IBAction)btnMusicClicked:(UIButton *)sender {
    sender.selected = !sender.selected;
    VIEWMANAGER.isPlayMusic = sender.selected;
    sender.selected ? [VIEWMANAGER pauseMusic] : [VIEWMANAGER playMusic];
}
- (IBAction)btnWallpaperClicked:(DashboardButton *)sender {
    [self pushToCategoriesController:CATEGORY_WALLPAPER];
}


- (IBAction)btnQuoteClicked:(DashboardButton *)sender {
    [self pushToCategoriesController:CATEGORY_QUOTES];
}

- (IBAction)btnInfoClicked:(id)sender {
    
}


-(void)pushToCategoriesController:(NSInteger)categoryType
{
    CategoriesViewController *categoryVC = (CategoriesViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"CategoriesViewController"];
    
    categoryVC.categoryType = categoryType;
    [self.navigationController pushViewController:categoryVC animated:YES];
}

@end
