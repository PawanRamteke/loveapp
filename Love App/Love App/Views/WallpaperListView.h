//
//  WallpaperListView.h
//  Love App
//
//  Created by Pawan Ramteke on 17/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WallpaperListView : UIView
-(instancetype)initWithFrame:(CGRect)frame categoryId:(double)catId;
@end
