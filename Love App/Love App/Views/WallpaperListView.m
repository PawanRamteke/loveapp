//
//  WallpaperListView.m
//  Love App
//
//  Created by Pawan Ramteke on 17/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "WallpaperListView.h"
#import "WallpaperCollectionCell.h"
#import "UIImageView+AFNetworking.h"
#import "ShareWallpaperController.h"
#import "CoreDataHandler.h"

@interface WallpaperListView()<UICollectionViewDataSource,UICollectionViewDelegate>
{
    UICollectionView *_collectionView;
    NSArray *arrWallpaperList;
    double categoryId;

}
@end

@implementation WallpaperListView

-(instancetype)initWithFrame:(CGRect)frame categoryId:(double)catId
{
    self = [super initWithFrame:frame];
    if (self) {
        
        categoryId = catId;
        [self setupLayout];
        [self getWallpaperListService];

    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupLayout];
        [self favouriteWallpaperService];
    }
    return self;
}

-(void)setupLayout
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    
    _collectionView = [[UICollectionView alloc]initWithFrame:self.bounds collectionViewLayout:layout];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    [self addSubview:_collectionView];
    [_collectionView registerClass:[WallpaperCollectionCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView setBackgroundColor:[UIColor clearColor]];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrWallpaperList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    WallpaperCollectionCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    WallpaperListModel *model = arrWallpaperList[indexPath.row];
    
    NSURL *url = [NSURL URLWithString:model.url];
    NSString *last = [NSString stringWithFormat:@"thumb/%@",[url lastPathComponent]];
    url =  [url URLByDeletingLastPathComponent];
    url = [url URLByAppendingPathComponent:last];
    
    [cell.imgView setImageWithURL:[NSURL URLWithString:url.absoluteString]];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ShareWallpaperController *shareWallpaperVC = [storyboard instantiateViewControllerWithIdentifier:@"ShareWallpaperController"];
    shareWallpaperVC.arrWallpaperList = arrWallpaperList;
    shareWallpaperVC.selIndex = indexPath.row;
    [[VIEWMANAGER topMostController].navigationController pushViewController:shareWallpaperVC animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((SCREEN_WIDTH - 10) / 2, 200);
}

-(void)getWallpaperListService
{
    NSDictionary *param = @{
                            @"api_token" : API_TOKEN,
                            @"category_id": @(categoryId),
                            @"language":ENGLISH
                            };
    [VIEWMANAGER showAcytivityIndicator];
    [REMOTE_API CallPOSTWebServiceWithParam:WALLPAPER_LIST_URL params:param sBlock:^(id responseObject) {
        arrWallpaperList = responseObject;
        [_collectionView reloadData];
        [VIEWMANAGER hideAcytivityIndicator];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        
    }];
}

-(void)favouriteWallpaperService
{
    [VIEWMANAGER showAcytivityIndicator];
    
    NSString *ids = [[CoreDataHandler getAllFavouriteWallpapers] componentsJoinedByString:@","];
    NSDictionary *param = @{
                            @"api_token":API_TOKEN,
                            @"type":@"WALLPAPER",
                            @"fav_ids":ids
                            };
    [REMOTE_API CallPOSTWebServiceWithParam:FAVOURITE_URL params:param sBlock:^(id responseObject) {
        [VIEWMANAGER hideAcytivityIndicator];
        arrWallpaperList = responseObject;
        [_collectionView reloadData];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [VIEWMANAGER hideAcytivityIndicator];
    }];
    
}

@end
