//
//  DashboardButton.h
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardButton : UIButton
@property(nonatomic,strong)IBInspectable UIImage *btnImage;
@property(nonatomic,strong)IBInspectable NSString *btnTitle;
@property(nonatomic,strong)IBInspectable NSString *btnSubTitle;
@property(nonatomic,strong)IBInspectable UIColor *topGradientColor;
@property(nonatomic,strong)IBInspectable UIColor *bottomGradientColor;

@end
