//
//  AppButton.m
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "AppGradientButton.h"

@interface AppGradientButton()
{
    CAGradientLayer *gradient;
}
@end

@implementation AppGradientButton

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self setGradient];
}

-(void)setGradient
{
    gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[self.topGradientColor CGColor], (id)[self.bottomGradientColor CGColor], nil];
    gradient.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.25f] CGColor];
    gradient.shadowOffset = CGSizeMake(2, 2.0f);
    gradient.shadowOpacity = 1.0f;
    gradient.shadowRadius = 10;
    gradient.masksToBounds = NO;
    gradient.cornerRadius = 25.0;
    [self.layer insertSublayer:gradient atIndex:0];
}
- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    self.backgroundColor = [UIColor clearColor];
    return self;
}

-(instancetype)init
{
    self = [super init];
    self.backgroundColor = [UIColor yellowColor];
    [self setGradient];
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    gradient.frame = self.bounds;
}

@end
