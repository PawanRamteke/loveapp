//
//  DashboardButton.m
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "DashboardButton.h"
#import "UIView+Autolayout.h"
#import "UIFont+App.h"

@interface DashboardButton()
{
    UIImageView *imgView;
    UILabel *lblTitle;
    UILabel *lblSubTitle;
    CAGradientLayer *gradient;
}
@end


@implementation DashboardButton

-(void)awakeFromNib
{
    [super awakeFromNib];
    imgView.image = self.btnImage;
    lblTitle.text = self.btnTitle;
    lblSubTitle.text = self.btnSubTitle;
    
    gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[self.topGradientColor CGColor], (id)[self.bottomGradientColor CGColor], nil];
    gradient.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.25f] CGColor];
    gradient.shadowOffset = CGSizeMake(2, 2.0f);
    gradient.shadowOpacity = 1.0f;
    gradient.shadowRadius = 10;
    gradient.masksToBounds = NO;
    gradient.cornerRadius = 25.0;

    [self.layer insertSublayer:gradient atIndex:0];

}

-(void)layoutSubviews
{
    gradient.frame = self.bounds;

}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    [self setupLayout];
    return self;
}
-(void)setupLayout
{
    self.backgroundColor = [UIColor clearColor];
    
    imgView = [[UIImageView alloc]init];
    imgView.layer.cornerRadius = 20;
    imgView.layer.masksToBounds = YES;
    [self addSubview:imgView];
    
    [imgView enableAutolayout];
    [imgView topMargin:5];
    [imgView bottomMargin:5];
    [imgView trailingMargin:5];
    [imgView fixWidth:40];
    
    UIView *viewBase = [[UIView alloc]init];
    viewBase.userInteractionEnabled = NO;
    [self addSubview:viewBase];
    
    [viewBase enableAutolayout];
    [viewBase leadingMargin:20];
    [viewBase addToLeft:0 ofView:imgView];
    [viewBase centerY];
    
    lblTitle = [[UILabel alloc]init];
    lblTitle.font = [UIFont boldSystemFontOfSize:18];
    lblTitle.textColor = [UIColor whiteColor];
    [viewBase addSubview:lblTitle];

    [lblTitle enableAutolayout];
    [lblTitle topMargin:0];
    [lblTitle leadingMargin:0];
    [lblTitle trailingMargin:0];
    
    lblSubTitle = [[UILabel alloc]init];
    lblSubTitle.font = [UIFont systemFontOfSize:10];
    lblSubTitle.numberOfLines = 0;
    lblSubTitle.textColor = [UIColor whiteColor];
    [viewBase addSubview:lblSubTitle];
    
    [lblSubTitle enableAutolayout];
    [lblSubTitle belowView:0 toView:lblTitle];
    [lblSubTitle leadingMargin:0];
    [lblSubTitle trailingMargin:0];
    [lblSubTitle bottomMargin:0];
}

@end
