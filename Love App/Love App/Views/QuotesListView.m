//
//  QuotesListView.m
//  Love App
//
//  Created by Pawan Ramteke on 17/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "QuotesListView.h"
#import "QuotesListTableCell.h"
#import "ShareQuotesController.h"
#import "CoreDataHandler.h"

@interface QuotesListView()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *tblViewList;
    NSArray *arrQuotesList;
    double categoryId;
}
@end
@implementation QuotesListView

-(instancetype)initWithFrame:(CGRect)frame categoryId:(double)catId
{
    self = [super initWithFrame:frame];
    if (self) {
        
        categoryId = catId;

        [self setupLayout];
        
        [self getQuotesListService];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupLayout];
        [self favouriteQuotesService];
    }
    return self;
}

-(void)setupLayout
{
    tblViewList = [[UITableView alloc]initWithFrame:self.bounds];
    tblViewList.delegate = self;
    tblViewList.dataSource = self;
    tblViewList.estimatedRowHeight = 100;
    tblViewList.rowHeight = UITableViewAutomaticDimension;
    tblViewList.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:tblViewList];
}

-(void)getQuotesListService
{
    NSDictionary *param = @{
                            @"api_token" : API_TOKEN,
                            @"category_id": @(categoryId),
                            @"language":ENGLISH
                            };
    [VIEWMANAGER showAcytivityIndicator];
    [REMOTE_API CallPOSTWebServiceWithParam:QUOTES_LIST_URL params:param sBlock:^(id responseObject) {
        arrQuotesList = responseObject;
        [tblViewList reloadData];
        [VIEWMANAGER hideAcytivityIndicator];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [VIEWMANAGER hideAcytivityIndicator];
    }];
}

-(void)favouriteQuotesService
{
    [VIEWMANAGER showAcytivityIndicator];
    NSString *ids = [[CoreDataHandler getAllFavouriteQuotes] componentsJoinedByString:@","];

    NSDictionary *param = @{
                            @"api_token":API_TOKEN,
                            @"type":@"QUOTE",
                            @"fav_ids":ids
                            };
    [REMOTE_API CallPOSTWebServiceWithParam:FAVOURITE_URL params:param sBlock:^(id responseObject) {
        [VIEWMANAGER hideAcytivityIndicator];
        arrQuotesList = responseObject;
        [tblViewList reloadData];
    } fBlock:^(NSString *customErrorMsg, NSInteger errorCode) {
        [VIEWMANAGER hideAcytivityIndicator];
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrQuotesList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    QuotesListTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellId"];
    if (cell == nil) {
        cell = [[QuotesListTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellId"];
    }
    
    QuotesListModel *model = arrQuotesList[indexPath.row];
    
    cell.lblQuotes.text = model.quote;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    ShareQuotesController *shareQuotesVC = [storyboard instantiateViewControllerWithIdentifier:@"ShareQuotesController"];
    shareQuotesVC.arrQuotes = arrQuotesList;
    shareQuotesVC.selectedIdx = indexPath.row;
    [[VIEWMANAGER topMostController].navigationController pushViewController:shareQuotesVC animated:YES];
}

@end
