//
//  CategoryModel.m
//
//  Created by Pawan Ramteke on 03/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "CategoryModel.h"


NSString *const kCategoryModelName = @"name";
NSString *const kCategoryModelId = @"id";
NSString *const kCategoryModelCreatedAt = @"created_at";
NSString *const kCategoryModelIsDelete = @"is_delete";
NSString *const kCategoryModelLanguage = @"language";
NSString *const kCategoryModelNameHindi = @"name_hindi";
NSString *const kCategoryModelUpdatedAt = @"updated_at";
NSString *const kCategoryModelSequenceNo = @"sequence_no";
NSString *const kCategoryModelAppName = @"app_name";
NSString *const kCategoryModelUrl = @"url";


@interface CategoryModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation CategoryModel

@synthesize name = _name;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize createdAt = _createdAt;
@synthesize isDelete = _isDelete;
@synthesize language = _language;
@synthesize nameHindi = _nameHindi;
@synthesize updatedAt = _updatedAt;
@synthesize sequenceNo = _sequenceNo;
@synthesize appName = _appName;
@synthesize url = _url;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.name = [self objectOrNilForKey:kCategoryModelName fromDictionary:dict];
            self.internalBaseClassIdentifier = [[self objectOrNilForKey:kCategoryModelId fromDictionary:dict] doubleValue];
            self.createdAt = [self objectOrNilForKey:kCategoryModelCreatedAt fromDictionary:dict];
            self.isDelete = [self objectOrNilForKey:kCategoryModelIsDelete fromDictionary:dict];
            self.language = [self objectOrNilForKey:kCategoryModelLanguage fromDictionary:dict];
            self.nameHindi = [self objectOrNilForKey:kCategoryModelNameHindi fromDictionary:dict];
            self.updatedAt = [self objectOrNilForKey:kCategoryModelUpdatedAt fromDictionary:dict];
            self.sequenceNo = [self objectOrNilForKey:kCategoryModelSequenceNo fromDictionary:dict];
            self.appName = [self objectOrNilForKey:kCategoryModelAppName fromDictionary:dict];
            self.url = [self objectOrNilForKey:kCategoryModelUrl fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.name forKey:kCategoryModelName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.internalBaseClassIdentifier] forKey:kCategoryModelId];
    [mutableDict setValue:self.createdAt forKey:kCategoryModelCreatedAt];
    [mutableDict setValue:self.isDelete forKey:kCategoryModelIsDelete];
    [mutableDict setValue:self.language forKey:kCategoryModelLanguage];
    [mutableDict setValue:self.nameHindi forKey:kCategoryModelNameHindi];
    [mutableDict setValue:self.updatedAt forKey:kCategoryModelUpdatedAt];
    [mutableDict setValue:self.sequenceNo forKey:kCategoryModelSequenceNo];
    [mutableDict setValue:self.appName forKey:kCategoryModelAppName];
    [mutableDict setValue:self.url forKey:kCategoryModelUrl];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.name = [aDecoder decodeObjectForKey:kCategoryModelName];
    self.internalBaseClassIdentifier = [aDecoder decodeDoubleForKey:kCategoryModelId];
    self.createdAt = [aDecoder decodeObjectForKey:kCategoryModelCreatedAt];
    self.isDelete = [aDecoder decodeObjectForKey:kCategoryModelIsDelete];
    self.language = [aDecoder decodeObjectForKey:kCategoryModelLanguage];
    self.nameHindi = [aDecoder decodeObjectForKey:kCategoryModelNameHindi];
    self.updatedAt = [aDecoder decodeObjectForKey:kCategoryModelUpdatedAt];
    self.sequenceNo = [aDecoder decodeObjectForKey:kCategoryModelSequenceNo];
    self.appName = [aDecoder decodeObjectForKey:kCategoryModelAppName];
    self.url = [aDecoder decodeObjectForKey:kCategoryModelUrl];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_name forKey:kCategoryModelName];
    [aCoder encodeDouble:_internalBaseClassIdentifier forKey:kCategoryModelId];
    [aCoder encodeObject:_createdAt forKey:kCategoryModelCreatedAt];
    [aCoder encodeObject:_isDelete forKey:kCategoryModelIsDelete];
    [aCoder encodeObject:_language forKey:kCategoryModelLanguage];
    [aCoder encodeObject:_nameHindi forKey:kCategoryModelNameHindi];
    [aCoder encodeObject:_updatedAt forKey:kCategoryModelUpdatedAt];
    [aCoder encodeObject:_sequenceNo forKey:kCategoryModelSequenceNo];
    [aCoder encodeObject:_appName forKey:kCategoryModelAppName];
    [aCoder encodeObject:_url forKey:kCategoryModelUrl];
}

- (id)copyWithZone:(NSZone *)zone
{
    CategoryModel *copy = [[CategoryModel alloc] init];
    
    if (copy) {
        
        copy.name = [self.name copyWithZone:zone];
        copy.internalBaseClassIdentifier = self.internalBaseClassIdentifier;
        copy.createdAt = [self.createdAt copyWithZone:zone];
        copy.isDelete = [self.isDelete copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.nameHindi = [self.nameHindi copyWithZone:zone];
        copy.updatedAt = [self.updatedAt copyWithZone:zone];
        copy.sequenceNo = [self.sequenceNo copyWithZone:zone];
        copy.appName = [self.appName copyWithZone:zone];
        copy.url = [self.url copyWithZone:zone];
    }
    
    return copy;
}

@end
