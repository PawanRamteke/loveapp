//
//  WallpaperListModel.h
//
//  Created by Pawan Ramteke on 03/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface WallpaperListModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *isDelete;
@property (nonatomic, assign) double internalBaseClassIdentifier;
@property (nonatomic, strong) NSString *updatedAt;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *categoryId;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, strong) NSString *quote;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
