//
//  WallpaperListModel.m
//
//  Created by Pawan Ramteke on 03/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "WallpaperListModel.h"


NSString *const kWallpaperListModelIsDelete = @"is_delete";
NSString *const kWallpaperListModelId = @"id";
NSString *const kWallpaperListModelUpdatedAt = @"updated_at";
NSString *const kWallpaperListModelUrl = @"url";
NSString *const kWallpaperListModelCategoryId = @"category_id";
NSString *const kWallpaperListModelCreatedAt = @"created_at";
NSString *const kWallpaperListModelQuote = @"quote";


@interface WallpaperListModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation WallpaperListModel

@synthesize isDelete = _isDelete;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize updatedAt = _updatedAt;
@synthesize url = _url;
@synthesize categoryId = _categoryId;
@synthesize createdAt = _createdAt;
@synthesize quote = _quote;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.isDelete = [self objectOrNilForKey:kWallpaperListModelIsDelete fromDictionary:dict];
            self.internalBaseClassIdentifier = [[self objectOrNilForKey:kWallpaperListModelId fromDictionary:dict] doubleValue];
            self.updatedAt = [self objectOrNilForKey:kWallpaperListModelUpdatedAt fromDictionary:dict];
            self.url = [self objectOrNilForKey:kWallpaperListModelUrl fromDictionary:dict];
            self.categoryId = [self objectOrNilForKey:kWallpaperListModelCategoryId fromDictionary:dict];
            self.createdAt = [self objectOrNilForKey:kWallpaperListModelCreatedAt fromDictionary:dict];
        self.quote = [self objectOrNilForKey:kWallpaperListModelQuote fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.isDelete forKey:kWallpaperListModelIsDelete];
    [mutableDict setValue:[NSNumber numberWithDouble:self.internalBaseClassIdentifier] forKey:kWallpaperListModelId];
    [mutableDict setValue:self.updatedAt forKey:kWallpaperListModelUpdatedAt];
    [mutableDict setValue:self.url forKey:kWallpaperListModelUrl];
    [mutableDict setValue:self.categoryId forKey:kWallpaperListModelCategoryId];
    [mutableDict setValue:self.createdAt forKey:kWallpaperListModelCreatedAt];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.isDelete = [aDecoder decodeObjectForKey:kWallpaperListModelIsDelete];
    self.internalBaseClassIdentifier = [aDecoder decodeDoubleForKey:kWallpaperListModelId];
    self.updatedAt = [aDecoder decodeObjectForKey:kWallpaperListModelUpdatedAt];
    self.url = [aDecoder decodeObjectForKey:kWallpaperListModelUrl];
    self.categoryId = [aDecoder decodeObjectForKey:kWallpaperListModelCategoryId];
    self.createdAt = [aDecoder decodeObjectForKey:kWallpaperListModelCreatedAt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_isDelete forKey:kWallpaperListModelIsDelete];
    [aCoder encodeDouble:_internalBaseClassIdentifier forKey:kWallpaperListModelId];
    [aCoder encodeObject:_updatedAt forKey:kWallpaperListModelUpdatedAt];
    [aCoder encodeObject:_url forKey:kWallpaperListModelUrl];
    [aCoder encodeObject:_categoryId forKey:kWallpaperListModelCategoryId];
    [aCoder encodeObject:_createdAt forKey:kWallpaperListModelCreatedAt];
}

- (id)copyWithZone:(NSZone *)zone
{
    WallpaperListModel *copy = [[WallpaperListModel alloc] init];
    
    if (copy) {

        copy.isDelete = [self.isDelete copyWithZone:zone];
        copy.internalBaseClassIdentifier = self.internalBaseClassIdentifier;
        copy.updatedAt = [self.updatedAt copyWithZone:zone];
        copy.url = [self.url copyWithZone:zone];
        copy.categoryId = [self.categoryId copyWithZone:zone];
        copy.createdAt = [self.createdAt copyWithZone:zone];
    }
    
    return copy;
}


@end
