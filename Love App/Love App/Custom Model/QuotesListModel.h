//
//  QuotesListModel.h
//
//  Created by Pawan Ramteke on 03/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface QuotesListModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) BOOL isDelete;
@property (nonatomic, assign) double internalBaseClassIdentifier;
@property (nonatomic, strong) NSString *updatedAt;
@property (nonatomic, assign) double categoryId;
@property (nonatomic, strong) NSString *quote;
@property (nonatomic, strong) NSString *createdAt;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
