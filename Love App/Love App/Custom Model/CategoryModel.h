//
//  WallpaperCategoryModel.h
//
//  Created by Pawan Ramteke on 03/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface CategoryModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) double internalBaseClassIdentifier;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, strong) NSString *isDelete;
@property (nonatomic, strong) NSString *language;
@property (nonatomic, strong) NSString *nameHindi;
@property (nonatomic, strong) NSString *updatedAt;
@property (nonatomic, strong) NSString *sequenceNo;
@property (nonatomic, strong) NSString *appName;
@property (nonatomic, strong) NSString *url;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
