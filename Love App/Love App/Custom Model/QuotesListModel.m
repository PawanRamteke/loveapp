//
//  QuotesListModel.m
//
//  Created by Pawan Ramteke on 03/02/18
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "QuotesListModel.h"


NSString *const kQuotesListModelIsDelete = @"is_delete";
NSString *const kQuotesListModelId = @"id";
NSString *const kQuotesListModelUpdatedAt = @"updated_at";
NSString *const kQuotesListModelCategoryId = @"category_id";
NSString *const kQuotesListModelQuote = @"quote";
NSString *const kQuotesListModelCreatedAt = @"created_at";


@interface QuotesListModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation QuotesListModel

@synthesize isDelete = _isDelete;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize updatedAt = _updatedAt;
@synthesize categoryId = _categoryId;
@synthesize quote = _quote;
@synthesize createdAt = _createdAt;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.isDelete = [[self objectOrNilForKey:kQuotesListModelIsDelete fromDictionary:dict] boolValue];
            self.internalBaseClassIdentifier = [[self objectOrNilForKey:kQuotesListModelId fromDictionary:dict] doubleValue];
            self.updatedAt = [self objectOrNilForKey:kQuotesListModelUpdatedAt fromDictionary:dict];
            self.categoryId = [[self objectOrNilForKey:kQuotesListModelCategoryId fromDictionary:dict] doubleValue];
            self.quote = [self objectOrNilForKey:kQuotesListModelQuote fromDictionary:dict];
            self.createdAt = [self objectOrNilForKey:kQuotesListModelCreatedAt fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithBool:self.isDelete] forKey:kQuotesListModelIsDelete];
    [mutableDict setValue:[NSNumber numberWithDouble:self.internalBaseClassIdentifier] forKey:kQuotesListModelId];
    [mutableDict setValue:self.updatedAt forKey:kQuotesListModelUpdatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.categoryId] forKey:kQuotesListModelCategoryId];
    [mutableDict setValue:self.quote forKey:kQuotesListModelQuote];
    [mutableDict setValue:self.createdAt forKey:kQuotesListModelCreatedAt];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.isDelete = [aDecoder decodeBoolForKey:kQuotesListModelIsDelete];
    self.internalBaseClassIdentifier = [aDecoder decodeDoubleForKey:kQuotesListModelId];
    self.updatedAt = [aDecoder decodeObjectForKey:kQuotesListModelUpdatedAt];
    self.categoryId = [aDecoder decodeDoubleForKey:kQuotesListModelCategoryId];
    self.quote = [aDecoder decodeObjectForKey:kQuotesListModelQuote];
    self.createdAt = [aDecoder decodeObjectForKey:kQuotesListModelCreatedAt];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeBool:_isDelete forKey:kQuotesListModelIsDelete];
    [aCoder encodeDouble:_internalBaseClassIdentifier forKey:kQuotesListModelId];
    [aCoder encodeObject:_updatedAt forKey:kQuotesListModelUpdatedAt];
    [aCoder encodeDouble:_categoryId forKey:kQuotesListModelCategoryId];
    [aCoder encodeObject:_quote forKey:kQuotesListModelQuote];
    [aCoder encodeObject:_createdAt forKey:kQuotesListModelCreatedAt];
}

- (id)copyWithZone:(NSZone *)zone
{
    QuotesListModel *copy = [[QuotesListModel alloc] init];
    
    if (copy) {

        copy.isDelete = self.isDelete;
        copy.internalBaseClassIdentifier = self.internalBaseClassIdentifier;
        copy.updatedAt = [self.updatedAt copyWithZone:zone];
        copy.categoryId = self.categoryId;
        copy.quote = [self.quote copyWithZone:zone];
        copy.createdAt = [self.createdAt copyWithZone:zone];
    }
    
    return copy;
}


@end
