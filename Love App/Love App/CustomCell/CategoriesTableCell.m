//
//  CategoriesTableCell.m
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "CategoriesTableCell.h"

@implementation CategoriesTableCell
@synthesize lblTitle;
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        lblTitle = [[UILabel alloc]init];
        lblTitle.layer.cornerRadius = 25.0;
        lblTitle.layer.shadowColor =[UIColor lightGrayColor].CGColor;
        lblTitle.layer.shadowOffset = CGSizeMake(3, 3);
        lblTitle.layer.shadowOpacity = 0.7;
        lblTitle.layer.shadowRadius = 4.0;
        lblTitle.layer.backgroundColor = [UIColor whiteColor].CGColor;
        lblTitle.font = [UIFont appFontRegular:22];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        [self addSubview:lblTitle];
        
        [lblTitle enableAutolayout];
        [lblTitle leadingMargin:40];
        [lblTitle trailingMargin:40];
        [lblTitle topMargin:10];
        [lblTitle bottomMargin:10];
        [lblTitle fixHeight:50];

    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    //lblTitle.layer.masksToBounds = YES;
}
@end
