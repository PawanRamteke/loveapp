//
//  WallpaperCollectionCell.m
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "WallpaperCollectionCell.h"

@implementation WallpaperCollectionCell
@synthesize imgView;
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        imgView = [[UIImageView alloc]init];
        imgView.backgroundColor = [UIColor redColor];
        imgView.layer.cornerRadius = 5;
        [self addSubview:imgView];
        
        [imgView enableAutolayout];
        [imgView leadingMargin:0];
        [imgView topMargin:0];
        [imgView trailingMargin:0];
        [imgView bottomMargin:0];
        
        
    }
    return self;
}
@end
