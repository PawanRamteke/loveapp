//
//  QuotesListTableCell.m
//  Love App
//
//  Created by Pawan Ramteke on 03/02/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

#import "QuotesListTableCell.h"

@implementation QuotesListTableCell
@synthesize lblQuotes;
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIView *baseView = [[UIView alloc]init];
        baseView.backgroundColor = [UIColor whiteColor];
        baseView.layer.shadowColor =[UIColor lightGrayColor].CGColor;
        baseView.layer.shadowOffset = CGSizeMake(3, 3);
        baseView.layer.shadowOpacity = 0.7;
        baseView.layer.shadowRadius = 4.0;
        [self addSubview:baseView];
        
        [baseView enableAutolayout];
        [baseView leadingMargin:10];
        [baseView trailingMargin:10];
        [baseView topMargin:10];
        [baseView bottomMargin:10];
        
        lblQuotes = [[UILabel alloc]init];
        lblQuotes.font = [UIFont appFontRegular:20];
        lblQuotes.numberOfLines = 3;
        lblQuotes.textAlignment = NSTextAlignmentCenter;
        [baseView addSubview:lblQuotes];
        
        [lblQuotes enableAutolayout];
        [lblQuotes leadingMargin:10];
        [lblQuotes trailingMargin:10];
        [lblQuotes topMargin:10];
        [lblQuotes bottomMargin:10];
        
    }
    return self;
}



@end
