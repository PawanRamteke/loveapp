//
//  RemoteAPI.h
//  SCB
//
//  Created by You on 14/07/17.
//  Copyright © 2017 You. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "CallbackTypes.h"
#import "ModelParser.h"
//Network error code
#define NO_INTERNET_CONNECTION  -1009

//Server Error code
#define UNAUTHORIZED_ACCESS     401

@interface RemoteAPI : NSObject



@property (nonatomic,strong)AFHTTPRequestOperationManager *operationManager;


- (void)CallGETWebServiceWithParam:(NSString*)apiUrl params:(NSDictionary*)params sBlock:(SUCCESSBLOCK)sBlock fBlock:(FAILUREBLOCK)fBlock;

-(void)CallPOSTWebServiceWithParam:(NSString*)apiUrl params:(NSDictionary*)params sBlock:(SUCCESSBLOCK)sBlock   fBlock:(FAILUREBLOCK)fBlock;

@end
