//
//  CallbackTypes.h
//  BusinessJodo
//
//  Created by Ketan Nandha on 03/03/17.
//  Copyright © 2017 KN. All rights reserved.
//

#import "General.h"

#ifndef CallbackTypes_h
#define CallbackTypes_h

typedef void (^arrayBlock)(NSArray * items);
typedef void (^dataBlock)(NSData * data);
typedef void (^arrayNextBlock)(NSArray*, NSURL*);
typedef void (^dictionaryBlock)(NSDictionary *dict);
typedef void (^stringBlock)(NSString *value);
typedef void (^idBlock)(id value);
typedef void (^errorBlock)(NSString *);
typedef void (^noArgBlock)();
typedef void (^nsErrorBlock)(NSError*);
typedef void (^httpErrorBlock)(NSInteger status, NSString* errorMessage);
typedef void (^integerBlock)(NSInteger value);
typedef void (^indexPathBlock)(NSIndexPath *indexPath);

typedef void(^SUCCESSBLOCK)(id responseObject);
typedef void(^FAILUREBLOCK)(NSString *customErrorMsg,NSInteger errorCode);

typedef void(^SelectedRowBlock)(id value,NSString *guid);

typedef void(^SelectedDataBlock)(NSMutableArray *arrData,NSMutableString *string);

#endif /* CallbackTypes_h */
