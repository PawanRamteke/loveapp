 //
//  RemoteAPI.m
//  SCB
//
//  Created by You on 14/07/17.
//  Copyright © 2017 You. All rights reserved.
//

#import "RemoteAPI.h"

@implementation RemoteAPI

-(id)init
{
    self = [super init];
    
    if (self)
    {
        self.operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
        
        self.operationManager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        [self.operationManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [self.operationManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];

    }
    return self;
}


- (void)CallGETWebServiceWithParam:(NSString*)apiUrl params:(NSDictionary*)params sBlock:(SUCCESSBLOCK)sBlock fBlock:(FAILUREBLOCK)fBlock;
{
    self.operationManager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    [self.operationManager GET:apiUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         sBlock([ModelParser parseModelWithAPIType:apiUrl responseData:responseObject]);
         
     } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
         
         //handle all other error seperately
         [self handleErrors:operation error:error fBlock:fBlock];
     }];
}

-(void)CallPOSTWebServiceWithParam:(NSString*)apiUrl params:(NSDictionary*)params sBlock:(SUCCESSBLOCK)sBlock   fBlock:(FAILUREBLOCK)fBlock
{
     self.operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [self.operationManager POST:apiUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
         
         if ([json[@"status"] isEqualToString:@"Failure"]) {
             fBlock(json[@"message"],400);
         }
         else{
             sBlock([ModelParser parseModelWithAPIType:apiUrl responseData:json[@"response"]]);
         }
         
         
         
     } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
         
         [self handleErrors:operation error:error fBlock:fBlock];
     }];
}

- (void)handleErrors:(AFHTTPRequestOperation*)operation  error:(NSError*)error     fBlock:(FAILUREBLOCK)fBlock
{
    //check first network errors like internet connection
    NSString *errorMsg = @"Please check internet connection and try again.";
    NSInteger errorCode = 0;
    if(error.code == NO_INTERNET_CONNECTION)
    {
        errorMsg =  error.userInfo[NSLocalizedDescriptionKey];
        errorCode = NO_INTERNET_CONNECTION;
    }
    else
    {
        //Some other error occured from server
        switch (operation.response.statusCode)
        {
            case UNAUTHORIZED_ACCESS:
            {
                errorMsg = @"Something went wrong, Please check and try again.";
                errorCode = UNAUTHORIZED_ACCESS;
            }
                break;
                
            default:
            {
                errorMsg = @"Something went wrong, Please check and try again.";
            }
                break;
        }
    }
    fBlock(errorMsg,errorCode);
}

@end
