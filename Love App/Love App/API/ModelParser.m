//
//  ModelParser.m
//  BusinessJodo
//
//  Created by Ketan Nandha on 03/03/17.
//  Copyright © 2017 KN. All rights reserved.
//

#import "ModelParser.h"

@implementation ModelParser

+(id)parseModelWithAPIType:(NSString *)apiURL responseData:(NSArray*)responseData;
{
    if ([apiURL isEqualToString:WALLPAPER_CATEGORY_URL] || [apiURL isEqualToString:QUOTES_CATEGORY_URL]) {
       return [self parseCategryData:responseData];
    }
    
    if ([apiURL isEqualToString:WALLPAPER_LIST_URL] || [apiURL isEqualToString:FAVOURITE_URL]) {
        return [self parseWallpaperListData:responseData];
    }
    
    if ([apiURL isEqualToString:QUOTES_LIST_URL]) {
        return [self parseQuotesListData:responseData];
    }
    
    return  nil;
}

+(id)parseCategryData:(NSArray *)responseData
{
    NSMutableArray *arrData = [NSMutableArray new];
    for (NSDictionary *dict in responseData) {
        CategoryModel *model = [CategoryModel modelObjectWithDictionary:dict];
        [arrData addObject:model];
    }
    return arrData;
}

+(id)parseWallpaperListData:(NSArray *)responseData
{
    NSMutableArray *arrData = [NSMutableArray new];
    for (NSDictionary *dict in responseData) {
        WallpaperListModel *model = [WallpaperListModel modelObjectWithDictionary:dict];
        [arrData addObject:model];
    }
    return arrData;
}

+(id)parseQuotesListData:(NSArray *)responseData
{
    NSMutableArray *arrData = [NSMutableArray new];
    for (NSDictionary *dict in responseData) {
        QuotesListModel *model = [QuotesListModel modelObjectWithDictionary:dict];
        [arrData addObject:model];
    }
    return arrData;
}

@end
